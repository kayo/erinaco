define(['../suit'], function(suit){
  function probeClasses(parent, data){
    var i = 0,
        classes = parent.children,
        Class,
        Subclass;

    for(; i < classes.length; ){
      Class = classes[i++];
      /* use recursion for deep search */
      Subclass = probeClasses(Class, data);
      if(Subclass){
        return Subclass;
      }
      if(Class.probe(data)){
        return Class;
      }
    }
    return null;
  }

  return suit.pro({
    $init: function(data, parent, name){
      this.name = name;
      this.data = data;
      this.parent = parent || null;
      this.iface = {};
      this.config(data);
    },
    face: function(node, done){
      var self = this;

      node.iface("org.illumium.erinaco.Basic", {
        method: {
          GetTitle: {
            res: {
              title: 's'
            }
          }
        }
      }, function(err, iface){
        if(err){
          done(err);
          return;
        }

        self.iface.basic = iface;
        iface.GetTitle(function(result){
          result(self.data.title);
        });

        done();
      });
    },
    bind: function(service, path, done){
      var self = this;

      service.node(path, function(err, node){
        if(err){
          done(err);
          return;
        }

        self.node = node;
        self.face(node, done);
      });
    },
    unbind: function(){
      for(var name in this.iface){
        this.iface[name].$();
      }
      this.iface = {};
      this.node.$();
    },
    load: function(data){},
    save: function(){return;},
    onch: function(cb){}
  }, {
    create: function(data, parent, name){
      var Class = probeClasses(this, data);

      if(Class){
        return new Class(data, parent, name);
      }

      throw new Error('Can\'t parse node: ' + JSON.stringify(data));
    }
  });
});
