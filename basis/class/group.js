define(['./basic'], function(Basic){
  return Basic({
    config: function(data){
      var pool = data.group,
          name;

      this.$ = {};

      for(name in pool){
        this.$[name] = Basic.create(pool[name], this, name);
      }
    },
    bind: function(service, path, done){
      var self = this;
      console.log('Binding group', path);
      Basic.prototype.bind.call(this, service, path, function(err){
        if(err){
          done(err);
          return;
        }

        var count = 1,
            error = [];

        function end(err){
          if(!--count){
            done(error.length ? new Error('Errors raised on binding: ' + error.join('\n')) : null);
          }
        }

        for(var name in self.$){
          ++count;
          self.$[name].bind(service, path + (path != '/' ? '/' : '') + name, end);
        }

        end();
      });
    },
    unbind: function(){
      Basic.prototype.unbind.call(this);
      for(var name in this.$){
        this.$[name].unbind();
      }
    },
    load: function(data){
      if('object' == typeof data){
        for(var name in this.$){ /* load */
          if(data[name] !== undefined){
            this.$[name].load(data[name]);
          }
        }
      }else{
        for(var name in this.$){ /* init */
          this.$[name].load(undefined);
        }
      }
    },
    save: function(){
      var data;
      for(var name in this.$){
        var val = this.$[name].save();
        if(val !== undefined){
          (data = data || {})[name] = val;
        }
      }
      return data;
    },
    onch: function(cb){
      for(var name in this.$){
        this.$[name].onch(cb);
      }
    }
  }, {
    probe: function(data){
      return 'group' in data;
    }
  });
});
