define(['./basic'], function(Basic){
  function invokeExtenders(clas, self, name){
    if(!clas){
      return;
    }

    invokeExtenders(clas.baseclass, self, name);

    var i = 0,
        exts = clas.extenders;

    if(!exts){
      return;
    }

    for(; i < exts.length; i++){
      if(exts[i][name] && (!exts[i].probe || exts[i].probe.call(self))){
        exts[i][name].call(self);
      }
    }
  }

  var introspection = {
    readable: {
      method: {
        GetValue: {
          res: {
            value: 'v'
          }
        },
        GetUnits: {
          res: {
            oneof: 's'
          }
        },
        GetRange: {
          res: {
            range: 'av'
          }
        },
        GetAvail: {
          res: {
            oneof: 'a{sv}'
          }
        }
      },
      signal: {
        OnValue: {
          arg: {
            value: 'v'
          }
        }
      }
    },
    writable: {
      method: {
        GetValue: {
          res: {
            value: 'v'
          }
        },
        SetValue: {
          arg: {
            value: 'v'
          },
          res: {
            state: 'b'
          }
        },
        GetDefault: {
          res: {
            value: 'v'
          }
        },
        GetUnits: {
          res: {
            oneof: 's'
          }
        },
        GetRange: {
          res: {
            range: 'av'
          }
        },
        GetAvail: {
          res: {
            oneof: 'a{sv}'
          }
        }
      },
      signal: {
        OnValue: {
          arg: {
            value: 'v'
          }
        }
      }
    }
  };

  return Basic({
    config: function(data){
      this.create(function(){
        this.invoke('create');
      });
    },
    face: function(node, done){
      var self = this;
      Basic.prototype.face.call(self, node, function(err){
        if(err){
          done(err);
          return;
        }

        node.iface("org.illumium.erinaco.Param", introspection[self.writable ? 'writable' : 'readable'], function(err, iface){
          if(err){
            done(err);
            return;
          }

          self.iface.param = iface;

          if(iface.GetValue){
            iface.GetValue(function(result){
              var value = self.$();
              result(value !== undefined ? value : '');
            });
          }

          if(iface.SetValue){
            iface.SetValue(function(value, result){
              var range = self.data.range,
                  oneof = self.data.oneof;

              if('object' == typeof range && range instanceof Array){
                if('number' == typeof range[0] && value < range[0] || 'number' == typeof range[1] && value > range[1]){
                  result(false);
                  return;
                }
              }

              if('object' == typeof oneof && !(oneof instanceof Array)){
                var bad = true;
                for(var key in oneof){
                  if(oneof[key] == value){
                    bad = false;
                  }
                }
                if(bad){
                  result(false);
                  return;
                }
              }

              self.$(value);
              result(true);
            });
          }

          if(iface.GetDefault){
            iface.GetDefault(function(result){
              result(self.data.value !== undefined ? self.data.value : '');
            });
          }

          if(iface.GetUnits){
            iface.GetUnits(function(result){
              result(self.data.units || '');
            });
          }

          if(iface.GetRange){
            iface.GetRange(function(result){
              var range = self.data.range;
              if(!('object' == typeof range && range instanceof Array)){
                range = [];
              }
              result(range);
            });
          }

          if(iface.GetAvail){
            iface.GetAvail(function(result){
              var oneof = self.data.oneof;
              if('object' != typeof oneof || oneof instanceof Array){
                oneof = {};
              }
              result(oneof);
            });
          }

          var step = 0;
          (function finalize(){
            if(self.$){
              self.signal = self.$(function(value){
                if(value !== undefined){
                  iface.OnValue && iface.OnValue(value);
                }
              });

              done();
            }else{
              if(step++ < 10){
                process.nextTick(finalize);
              }else{
                //throw new Error('Parameter');
              }
            }
          })();
        });
      });
    },
    bind: function(service, path, done){
      console.log('Binding param', path);
      Basic.prototype.bind.call(this, service, path, done);
    },
    destroy: function(){
      this.invoke('remove');
      this.remove();
    },
    create: function(cb){
      cb.call(this);
    },
    remove: function(cb){
      if(this.signal){
        this.signal();
      }
      cb.call(this);
    },
    invoke: function(name){
      invokeExtenders(this.constructor, this, name);
    }
  }, {
    probe: function(/*data*/){
      /* cannot create instance of this virtual class */
      return false;
    },
    extenders: [],
    extend: function(exts){
      this.extenders.push(exts);
    }
  });
});
