define(['./param', 'observable'], function(Param, Observable){
  var str_pfx = {'"':0,"'":0},
      js_cnst = {'true':0,'false':0};

  function processCode(data, deps){
    return data.toString().replace(/("(?:\"|[^"])*"|'(?:\'|[^'])*'|[a-zA-Z_$][a-zA-Z0-9_$\.]*)/g, function(_, param){
      return param.charAt(0) in str_pfx || param in js_cnst ? param : (deps[param] = param.replace(/\./g, '__'));
    });
  }

  function compileSolve(data, deps){
    var i = 0,
        res = '',
        cond,
        then;

    if(data.length < 1){
      throw new Error('Missing any cases.');
    }

    if(!(data.length % 2)){
      throw new Error('Missing default case.');
    }

    for(; i < data.length; ){
      if(i < data.length - 1){
        cond = data[i++];
        then = data[i++];
      }else{
        cond = null;
        if(i < data.length){
          then = data[i++];
        }else{
          then = 'null';
        }
      }
      if(then instanceof Array){
        then = compileSolve(then, deps);
      }else{
        then = processCode(then, deps);
      }
      if(cond){
        res += '(' + processCode(cond, deps) + ')?(' + then + '):';
      }else{
        res += '(' + then + ')';
      }
    }
    return res;
  }

  function getDependency(node, name){
    name = name.split(/\./);
    /* goes to node */
    for(; node.parent; ){
      node = node.parent;
    }
    /* goes to target */
    for(var i = 0; i < name.length; ){
      node = node.$[name[i++]];
      if(!node){
        return null;
      }
    }
    return node.$;
  }

  return Param({
    readable: true,
    writable: false,
    create: function(cb){
      var self = this,
          data = self.data,
          deps = {},
          deps$ = {},
          name,
          code = compileSolve(data.solve, deps);

      var step = 10;
      (function finalize(){
        var unresolved = [];

        for(name in deps){
          if(!(deps$[name] || (deps$[name] = getDependency(self, name)))){
            unresolved.push(name);
          }
        }

        if(unresolved.length){
          if(step--){
            process.nextTick(finalize);
            return;
          }
          throw new Error('Problem with dependencies: ' + unresolved.join(', '));
        }

        var deps_list = [],
            args_list = [],
            func;

        for(name in deps){
          deps_list.push(deps$[name]);
          args_list.push(deps[name]);
        }

        try{
          func = new Function(args_list.join(','), 'return ' + code + ';');
        }catch(er){
          cb.call(self, new Error('Invalid solve code: ' + er));
        }
        self.$ = Observable.compute(deps_list, func);

        cb.call(self);
      })();
    }
  }, {
    probe: function(data){
      return 'solve' in data;
    }
  });
});
