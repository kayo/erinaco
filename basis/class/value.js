define(['./param', 'observable'], function(Param, Observable){
  return Param({
    readable: true,
    writable: true,
    create: function(cb){
      this.$ = Observable.signal();
      if(this.data.perms){
        this.readable = this.data.perms.indexOf('r') > -1;
        this.writable = this.data.perms.indexOf('w') > -1;
      }
      cb.call(this);
    },
    load: function(data){
      if(this.writable){
        if(data !== undefined){
          this.$(data);
        }else if(this.data.value !== undefined){
          this.$(this.data.value);
        }
      }
    },
    save: function(){
      if(this.writable){
        return this.$();
      }
      return undefined;
    },
    onch: function(cb){
      if(this.writable){
        this.$(cb);
      }
    }
  }, {
    probe: function(data){
      return !('group' in data || 'solve' in data);
    }
  });
});
