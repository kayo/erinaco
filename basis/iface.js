define(['./class/param', './local!.', 'debug'], function(Param, Local, Debug){
  var debug = Debug('iface'),
      ifaces = {},
      units = {},
      round = Math.round,
      abs = Math.abs;

  units[''] = 1;

  units.S = 1000;
  units.M = units.S * 60;
  units.H = units.M * 60;

  units.d = units.H * 24;
  units.y = units.d * 365;
  units.q = units.y / 4;
  units.m = units.q / 3;

  function parseDelay(delay){
    return delay ? (function(_, v, u){
      return _ ? round(parseFloat(v) * units[u]) : 0;
    }).apply(null, delay.toString().match(/^([\d\.]+)([yqmdHMS]?)$/)) : 0;
  }

  Param.extend({
    probe: function(){
      return 'iface' in this.data;
    },
    create: function(){
      var self = this,
          data = self.data,
          I = ifaces[data.iface];

      if(I){
        finalize(I);
      }else{
        Local.require(['./iface/' + data.iface], finalize);
      }

      function finalize(I){
        var i = self._ = I.crt ? I.crt(data) : new I(data),
            rpoll = parseDelay(data.rpoll),
            wpoll = parseDelay(data.wpoll);

        if(self.readable && i.on){
          i.on(function(val){
            debug('On:%s Iface:%s Value:%s', self.name, data.iface, val);
            self.$(val);
          });
        }
        
        if(self.readable && i.get){
          if(rpoll){
            (function polling(){
              i.get(function(err, cur){
                if(err){
                  console.error(err);
                }else{
                  debug('Read:%s Iface:%s Delay:%dms Value:%s', self.name, data.iface, rpoll, cur);

                  var old = self.$();

                  if('number' == typeof data.delta){
                    if(abs(cur - old) < data.delta){
                      cur = old;
                    }
                  }

                  if(cur != old){
                    self.$(cur);
                  }
                }

                self._g = setTimeout(polling, rpoll);
              });
            })();
          }
        }

        if(self.writable && !i.set){
          // forcing to false
          self.writable = false;
        }
        
        if(i.set){
          if(wpoll){
            (function polling(){
              if(self._v === undefined){
                self._s = setTimeout(polling, wpoll);
                return;
              }

              i.set(self._v, function(err){
                if(err){
                  console.error(err);
                }

                debug('Write:%s Iface:%s Delay:%dms Value:%s', self.name, data.iface, rpoll, self._v);

                self._s = setTimeout(polling, wpoll);
              });
            })();
          }
          self._l = self.$(function(val){
            if(val === undefined){
              return;
            }
            self._v = val;

            if(self._t){
              clearTimeout(self._t);
              self._t = null;
            }

            self._t = setTimeout(function(){ /* throttling */
              i.set(val, function(err){
                if(err){
                  console.error(err);
                  return;
                }
              });
              self._t = null;
            }, 100);
          });
        }
      }
    },
    remove: function(){
      /* remove set listener */
      if(self._l){
        self._l();
      }
      /* remove get poller */
      if(self._g){
        clearTimeout(self._g);
      }
      /* remove set poller */
      if(self._s){
        clearTimeout(self._s);
      }
    }
  });

  return ifaces;
});
