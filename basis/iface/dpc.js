define(['ndbus'], function(DBus){
  var bus = DBus(true);

  function PAR(opts){
    var self = this;

    self._dev_name = opts.hwdev;
    self._par_name = opts.param;

    self._dev_path = '/' + self._dev_name;
    self._par_path = self._dev_path + '/' + self._par_name;

    self.connect();
  }

  PAR.prototype = {
    connect: function(){
      var self = this;

      if(!self._dev_iface){
        bus.proxy('org.illumium.dpc', self._dev_path, 'org.illumium.dpc.Device', function(err, dev){
          if(err){
            setTimeout(function(){
              self.connect();
            }, 1000 * 60 * 10);
            return;
          }

          self._dev_iface = dev;

          self._dev_iface.StatusChanged.on(function(status){
            self.toggle(status == 'online');
          });

          self.connect();
        });
      }else if(!self._par_iface){
        bus.proxy('org.illumium.dpc', self._par_path, 'org.illumium.dpc.Param', function(err, par){
          if(err){
            return;
          }

          self._par_iface = par;
          self._online = true;
        });
      }
    },
    toggle: function(online){
      var self = this;

      if(online && !self._online){
        if(self._set_val !== undefined){
          self._par_iface.SetValue(self._set_val, function(err){
            if(!err){
              self._set_val = undefined;
            }
          });
        }
      }

      self._online = online;
    },
    get: function(ret){
      var self = this;

      if(!self._online){
        ret(new Error('offline'));
        return;
      }

      self._par_iface.GetValue(ret);
    },
    set: function(val, ret){
      var self = this;

      if(!self._online){
        ret(new Error('offline'));
        return;
      }

      self._par_iface.SetValue(val, function(err){
        if(err){
          self._set_val = val;
          ret(err);
          return;
        }

        ret();
      });
    }
  };

  return PAR;
});
