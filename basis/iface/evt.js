define(function(){
  var timer,
      time_pool = [],
      time_value = time_init(),
      time_rpoll = {
        year: '1y',
        mon: '1m',
        date: '1d',
        day: '1d',
        hour: '1H',
        min: '1M',
        sec: '1S'
      };

  function time_init(){
    var d = new Date(),
        dow = d.getDay();
    if(!dow){
      dow = 7;
    }
    return {
      year: d.getFullYear(), /* year xxxx */
      mon:  d.getMonth() + 1, /* month [1, 12] */
      date: d.getDate(), /* day of month [1, 31] */
      day:  dow, /* day of week [1-mon, ..., 7-sun] */
      hour: d.getHours(), /* hours [0, 23] */
      min:  d.getMinutes(), /* minutes [0, 59] */
      sec:  d.getSeconds() /* seconds [0, 59] */
    };
  }

  function time_tick(){
    time_value = time_init();
    for(var i = 0; i < time_pool.length; i++){
      time_pool[i].cb(time_value[time_pool[i].units]);
    }
  }

  function Time(opts){
    if(!timer){
      timer = setInterval(time_tick, 1000);
    }
    if(!opts.units || !(opts.units in time_value)){
      opts.units = 'sec';
    }
    this.units = opts.units;
  }
  
  Time.prototype = {
    get: function(ret){
      ret(null, time_value[this.units]);
    },
    on: function(cb){
      this.cb = cb;
      if(this.cb){
        time_pool.push(this);
      }else{
        var idx = time_pool.indexOf(this);
        if(idx > -1){
          time_pool.splice(idx, 1);
        }
      }
    }
  };

  return function(opts){
    switch(opts.event){
      case 'time': return new Time(opts);
    }
  };
});
