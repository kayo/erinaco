define(['fs'], function(FS){
  var gpio = {
    export: '/sys/class/gpio/export',
    unexport: '/sys/class/gpio/unexport',
    direction: function(id){ return '/sys/class/gpio/gpio' + id + '/direction'; },
    value: function(id){ return '/sys/class/gpio/gpio' + id + '/value'; }
  };

  function GPO(data){
    this._ports = (data.ports || []).map(function(id, n){
      return {
        id: id,
        export: false,
        output: false
      };
    });

    this._maxval = (1 << this._ports.length) - 1;
    this._inited = false;

    this.init();
  }

  GPO.prototype = {
    init: function(){
      var self = this,
          cops = 0;

      function end(){
        cops++;
        if(cops == self._ports.length){
          if(self._val !== undefined){
            self.set(self._val, function(){});
          }
        }
      }

      self._ports.forEach(function(port, n){
        port.dev_dir = gpio.direction(port.id);
        port.dev_val = gpio.value(port.id);
        
        FS.exists(port.dev_dir, function(exists){
          if(exists){
            set_dir();
          }else{
            set_pin();
          }
        });

        function set_pin(){
          FS.writeFile(gpio.export, '' + port.id + '\n', function(err){
            if(err){
              console.warn('GPOut export error: ', err);
              end();
              return;
            }

            port.export = true;
            set_dir();
          });
        }

        function set_dir(){
          FS.readFile(port.dev_dir, 'ascii', function(err, data){
            if(err){
              console.warn('GPOut Error: ', err);
              end();
              return;
            }

            port.old_dir = data;

            if(port.old_dir != 'out'){
              FS.writeFile(port.dev_dir, 'out', function(err){
                if(err){
                  console.warn('GPOut direction error: ', err);
                  return;
                }
            
                port.output = true;
                end();
              });
            }else{
              port.output = true;
              end();
            }
          });
        }
      });
    },
    done: function(){
      self._ports.forEach(function(port, n){
        if(port.output){
          FS.writeFile(port.dev_dir, port.old_dir, function(err){
            unexport();
          });
        }else{
          unexport();
        }

        function unexport(){
          FS.writeFile(gpio.unexport, '' + port.id + '\n', function(err){
            if(err){
              return;
            }
          });
        }
      });
    },
    set: function(val, ret){
      var self = this,
          errs = [],
          cops = 0;

      if(!self._ports.length){
        ret();
      }

      if(val < 0){
        val = 0;
      }

      if(val > self._maxval){
        val = self._maxval;
      }

      self._val = val;

      self._ports.forEach(function(port, n){
        var bit = ((val >> n) & 1).toString();
        
        if(port.output){
          FS.writeFile(port.dev_val, bit, end);
        }else{
          end();
        }
      });

      function end(err){
        if(err){
          errs.push(err);
        }
        cops ++;
        if(cops == self._ports.length){
          if(errs.length){
            ret(new Error('GPOut Error:' + errs.join('\n')));
          }else{
            ret();
          }
        }
      }
    }
  };

  return GPO;
});
