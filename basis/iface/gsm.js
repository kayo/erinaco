define(['../../voiceui/ofono'], function(Ofono){
  var ofono = new Ofono(),
      modem;

  ofono.on('modem', function(modem_){
    modem = modem_;
    modem.state(true);
  });

  function withModem(cb){
    if(modem){
      cb();
    }else{
      ofono.once('modem', cb);
    }
  }

  function GPRS(opts){}

  GPRS.prototype = {
    set: function(val, ret){
      var self = this;

      withModem(function(){
        modem.gprs(val);
        ret();
      });
    }
  };

  function GPRSStatus(opts){}

  GPRSStatus.prototype = {
    get: function(ret){
      var self = this;

      withModem(function(){
        ret(null, modem.gprs());
      });
    }
  };

  function USSD(opts){
    var self = this;

    self._req = opts.req;
    self._res = opts.res;
  }

  USSD.prototype = {
    get: function(ret){
      var self = this;

      withModem(function(){
        modem.ussd(self._req, function(err, res){
          if(err){
            ret(err);
            return;
          }

          if('function' == typeof self._res){
            ret(null, self._res(res));
          }else{
            var m = res.match(self._res);
            ret(null, m[1]);
          }
        });
      });
    }
  };

  return function(opts){
    if(opts.type == 'ussd'){
      return new USSD(opts);
    }else{
      switch(opts.param){
        case 'gprs': return new GPRS(opts);
        case 'gprs.status': return new GPRSStatus(opts);
      }
    }
    return {};
  };
});
