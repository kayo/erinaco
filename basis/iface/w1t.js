define(['fs'], function(FS){
  function W1T(data){
    this._dev = '/sys/bus/w1/devices/' + data.ucode + '/w1_slave';
  }

  W1T.prototype = {
    get: function(ret){
      FS.readFile(this._dev, 'ascii', function(err, data){
        if(err){
          ret(err);
          return;
        }

        var valid = false,
            value;

        data = data.split('\n');

        for(var i = 0; i < data.length; i++){
          var l = data[i].split(/\s+/);
          
          if(l.length < 9){
            continue;
          }

          if(l[9] == ':'){
            valid = l[11] == 'YES';
          }else{
            var d = l[9].split('=');

            if(d[0] == 't'){
              value = 0.001 * parseInt(d[1], 10);
            }
          }
        }

        if(valid){
          ret(null, value);
        }else{
          ret(new Error('CRC Error'));
        }
      });
    }
  };

  return W1T;
});
