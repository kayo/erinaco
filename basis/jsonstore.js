define(['fs'], function(FS){
  function dummy(){}

  function Store(path, dsto){
    this.path = path;
    this.dsto = dsto;
    this._sop = null;
  }

  Store.prototype = {
    load: function(cb){
      cb = cb || dummy;
      FS.readFile(this.path, function(er, data){
        if(er){
          cb(er);
          return;
        }
        if(!data){
          cb(null, {});
          return;
        }
        try{
          data = JSON.parse(data.toString('utf8'));
        }catch(er){
          cb(er);
          return;
        }
        cb(null, data);
      });
    },
    save: function(data, cb){
      var self = this;

      if(data === undefined){
        return;
      }

      try{
        data = JSON.stringify(data, null, '  ');
      }catch(er){
        return;
      }

      function save(){
        FS.writeFile(self.path, data, cb);
      }

      if(self.dsto){
        if(self._sop){
          clearTimeout(self._sop);
          self._sop = null;
        }
        self._sop = setTimeout(save, self.dsto);
      }else{
        save();
      }
    }
  };

  return Store;
});
