define(['ndbus', './jsonstore', './class', './iface', 'CONFIG/struct'], function(DBus, Store, Class, Ifaces, struct){
  function Control(opt){
    this.bus = DBus(opt.bus == ':system:' ? true : opt.bus ? opt.bus : false);

    this.store = new Store((opt.data_path || '.') + '/params.json', 5000);
  }

  Control.prototype = {
    start: function(done){
      var self = this,
          pool = self.$ = Class.create(struct);

      self.store.load(function(err, data){
        if(!err && data){
          console.log('pool load');
          pool.load(data);
        }else{
          console.log('pool init');
          pool.load(undefined);
        }

        pool.onch(function(){
          self.store.save(pool.save(), function(){
            console.log('pool save');
          });
        });
      });

      self.bus.service("org.illumium.erinaco", function(err, service){
        if(err){
          done(err);
        }

        self.service = service;
        pool.bind(service, '/', done);
      });
    },
    stop: function(done){
      var self = this;

      self.$.unbind();
      self.service.$();
      done();
    }
  };

  return Control;
});
