define(function(){
  var slice = Array.prototype.slice,

  client = typeof window != 'undefined',
  server = typeof process != 'undefined',

  /**
   * Check instance of.
   *
   * @param inst {Object} target instance
   * @param base {Function|Arguments} target constructor
   * @returns {Boolean}
   */
  iof = function(inst, base){
    return (inst && inst.prototype || inst) instanceof (base.callee || base);
  },

  /**
   * Get or check variable type.
   *
   * @param arg {Any} target variable
   * @param type {String} optional target type(s)
   * @returns {Boolean|String} type correspondence or type code
   *
   * Available type codes:
   *   u - undefined;
   *   b - boolean;
   *   s - string;
   *   n - number;
   *   f - function;
   *   a - array;
   *   o - object (not array);
   *   _ - null;
   *
   * If type(s) presented, type checking will be performed with return true if type corresponds. For example:
   *   is(a, 's') - true if a is string
   *   is(a, 'fo') - true if a is function or object
   *
   * If type(s) ommited, type code will be returned. For example:
   *   is(a) - 's' if a is string, 'a' if a is array, etc
   */
  is = function(arg, type){
    arg = arg === null ? '_' : (iof(arg, Array) ? 'a' : (typeof arg).charAt(0));

    if(type){
      return type.indexOf(arg) > -1;
    }
    return arg;
  },

  /**
   * Extract subset of arguments as an array.
   *
   * @param args {Arguments} an arguments object
   * @param start {Number} optional first argument index
   * @param end {Number} optional last argument index
   * @returns {Array} array of arguments
   *
   * Extract argument value by type.
   *
   * @param args {Arguments} an arguments object
   * @param type {String} argument type
   * @param default {Any} optional default value
   * @returns {Any} argument value
   *
   */
  arg = function(args, start, end){
    if(is(start, 's')){
      var i = 0,
      n = start.indexOf('#') > -1;
      for(; i < args.length; i++){
        if(is(args[i], start)){
          return n ? i : args[i];
        }
      }
      return n ? -1 : end;
    }
    return slice.apply(args, [start || 0, end || args.length]);
  },

  voidy = {}, /* empty object */
  dummy = function(){}, /* dummy function */

  trim_re_c = /\s*$/,
  trim_re_o = /^\s*/,

  camel_re = /\-([a-zA-Z])/g,
  camel_re_un = /\-?([A-Z])/g,
  camel_re_de = /^\-/,

  freezing = 0,
  ntick = !freezing && (server
                        && process.nextTick
                        || client
                        && (window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame))
    || function(callback){
      setTimeout(callback, freezing);
    },

  suit = {
    c: client,
    s: server,

    U: undefined,
    N: null,
    A: [], /* empty array */
    O: voidy,
    S: '', /* empty string */
    F: dummy,

    trim: function(str){
      return str.replace(trim_re_o, '').replace(trim_re_c, '');
    },

    camel: function(str, opt){
      opt = opt || '^';
      var unc = opt.search(/\-/) > -1,
      opn = opt.search(/\*/) > -1,
      reg = opt.search(/\^/) > -1;

      return unc ? str.replace(camel_re_un, function(str, first){
        return '-' + (reg ? first.toUpperCase() : first.toLowerCase());
      }).replace(opn ? '' : camel_re_de, '') : ((opn ? '' : '-') + str).replace(camel_re, function(str, first){
        return reg ? first.toUpperCase() : first.toLowerCase();
      });
    },

    iof: iof,
    is: is,
    arg: arg,

    empty: function(data){
      if(is(data, 'snb')){
        return !data;
      }else if(is(data, 'a')){
        return !data.length;
      }else if(is(data, 'o')){
        for(var n in data){
          if(!(n in voidy)){
            return false;
          }
        }
      }
      return true;
    },

    /**
     * Mixin object's properties.
     *
     * @param dst {Object} destination object
     * @param src {Object} source object
     * @param force {Boolean} optional force overwrite
     */
    mix: function(dst, src, force){
      for(var i in src){
        if(!(i in voidy) && (!(i in dst) || force)){
          dst[i] = src[i];
        }
      }
      return dst;
    },
    /**
     * Converts data:
     * {<key>:{<subkey>:<subval>}}
     * with schema:
     * {'keysname':0,'valsname':{'subkeysname':0,'subvalsname':1}}
     * to:
     * [{keysname:<key>,valsname:[{subkeysname:<subkey>,subvalsname:<subval>}]}]
     */
    norm: function(obj, scm){
      var res = [], key, entry, field, value, sub;
      for(key in scm){
        if(scm[key] == 0){
          field = key;
        }else{
          value = key;
          sub = scm[key];
        }
      }
      for(key in obj){
        entry = {};
        entry[field] = key;
        entry[value] = is(sub, 'o') ? suit.norm(obj[key], sub) : obj[key];
        res.push(entry);
      }
      return res;
    },

    pipe: function(){
      var funcs = arg(arguments),
      c = -1,
      i;
      for(i = 0; i < funcs.length; i++){
        if(!is(funcs[i], 'f')){
          if(is(funcs[i], 'n')){ /* number of callback argument */
            c = funcs[i];
          }
          funcs.splice(i--, 1); /* removes non-function */
        }
      }
      return function(){
        var self = this,
        args = arguments,
        c = c > -1 ? c : arg(args, 'f#'),
        cb = c > -1 ? args[c] : null,
        n = 0,
        ret,
        proc = function(){
          if(n == funcs.length){
            if(is(cb, 'f')){
              cb.apply.apply(cb, ret);
            }
            return;
          }
          var a = arguments;
          ret = [this, a];
          a = arg(args);
          if(c > -1){
            a[c] = proc;
          }else{
            a.unshift(proc);
          }
          funcs[n++].apply(self, a);
        };
        proc();
      };
    },

    New: function(){ /* constructor wrapper */
      var C = this,
      A = arguments,
      F = function(){
        return C.apply(this, A);
      };
      if(typeof C.callee == 'function'){
        C = C.callee;
      }
      F.prototype = C.prototype;
      return new F();
    },

    con: function(C, A){ /* constructor caller */
      return suit.New.apply(C, A);
    },

    ext: function(C, B){ /* simple inheritance */
      var F = function(){};
      F.prototype = B.prototype;
      C.prototype = new F();
      C.baseclass = B;
      B.children.push(C);
    },

    sub: function(base, data){ /* prototype-based extending */
      if(arguments.length < 2){
        data = base;
        base = 0;
      }

      var self = is(this, 'f') ? this : function(){};
      if(base){
        suit.ext(self, base);
      }
      if(is(data, 'f')){
        data = data(base && base.prototype);
      }
      if(data){
        suit.mix(self.prototype, data, true);
      }
      self.prototype.constructor = self;
      self.children = [];
      return self;
    },

    wrap: function(self, func){
      self = arg(arguments, 'o');
      func = arg(arguments, 'sf');
      if(is(func, 's')){
        func = self[func];
      }
      func = func || dummy;
      return function(){
        return func.apply(self, arguments);
      };
    },

    all: function(){
      var funcs = arg(arguments);
      for(var i = 0; i < funcs.length; i++){
        if(!is(funcs[i], 'f')){
          funcs.splice(i--, 1);
        }
      }
      return function(){
        for(var i = 0; i < funcs.length; i++){
          funcs[i].apply(this, arguments);
        }
      };
    },

    nxt: function(self, func, args){
      if(!is(self, 'o')){
        args = func;
        func = self;
        self = this;
      }
      ntick(function(){
        if(self && is(func, 's')){
          func = self[func];
        }
        if(is(func, 'f')){
          if(self && is(args, 's')){
            args = self[args];
          }
          if(is(args, 'f')){
            args = args.call(self);
          }
          func.apply(self || this, args || []);
        }
      });
    },

    /**
     * Converts hex string to numeric array
     */

    s2h: function(str, base){
      base = base || 2;
      var ret = [], i = 0, c = 0;
      for(; c < str.length; c += base){
        ret[i++] = parseInt(str.substr(c, base), 16);
      }
      return ret;
    },

    h2s: function(arr, base){
      base = base || 2;
      var ret = '', i = 0, s;
      for(; i < arr.length; ){
        s = arr[i++].toString(16);
        for(; base > s.length; s = '0' + s);
        ret += s;
      }
      return ret;
    },

    /*
     * Data to Numeric Array
     */

    b2n: function(data, base, enc){
      return suit.s2h(Buffer(data, enc || 'base64').toString('hex'), base);
    },

    n2b: function(arr, base, enc){
      return Buffer(suit.h2s(arr, base), 'hex').toString(enc || 'base64');
    },

    /*
     b2n: function(data, base, enc){
      base = base || 2;
      var buf = new Buffer(data, enc || 'base64').toString('hex'),
      ret = [], i = 0;
      for(; i < buf.length; i += base){
        //ret.push(parseInt(buf.slice(i, i + base).toString('hex'), 10));
        ret.push(parseInt(buf.substr(i, base), 10));
      }
      return ret;
    },

    n2b: function(arr, base, enc){

    },
     */

    /**
     * Inverts numeric array or object
     */
    i2v: function(arr){
      var ret, i;
      if(is(arr, 'a')){
        for(ret = [], i = 0;
            i < arr.length;){
          ret[arr[i]] = i++;
        }
      }else if(is(arr, 'o')){
        ret = {};
        for(i in arr){
          if(!(i in voidy)){
            ret[arr[i]] = i;
          }
        }
      }
      return ret;
    },

    err: function(msg, opt){
      var e = new Error(msg);
      suit.mix(e, opt, true);
      return e;
    }
  },

  pro = function(self, args){
    if(!iof(self, args)){ /* called as function */
      return suit.mix(suit.sub.call(function(){
        return pro(this, arguments);
      }, args.callee, args[0]), args[1] || {});
    } /* called as constructor */
    if(is(self.$init, 'f')){
      self.$init.apply(self, args);
    }
    return self;
  };

  suit.pro = function(){
    return pro(this, arguments);
  };

  suit.pro.prototype = {
    $init: dummy
  };

  suit.pro.children = [];

  /* Chain  */
  var Chain = suit.chain = function(self, opts){
    if(suit.iof(this, arguments)){
      this._slf = self;
      this._que = [];
      suit.mix(this, opts || {});
      this.next();
    }else{
      return new Chain(self || this, opts);
    }
  };

  Chain.prototype = {
    sub: function(){
      var chain = new Chain(this._slf, this);
      chain.sup = this;
      return chain;
    },
    queue: function(op, args){ /* queue */
      this._que.push([op, args]);
    },
    next: function(){ /* deque */
      suit.nxt(this, function(){
        if(this._que.length){
          this._lop = this._que.shift();
          this[this._lop[0]].apply(this, this._lop[1]);
        }
      });
    },
    repeat: function(){ /* undeque */
      this._que.unshift(this._lop);
      this.next();
    },
    _u: function(body){
      body.call(this._slf, this, this._par);
    },
    unit: function(){
      this.queue('_u', arguments);
      return this;
    }
  };

  return suit;
});
