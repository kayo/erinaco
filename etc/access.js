define({
  username: {
    lang: 'russian',
    perm: [
      'textui.console',
      'textui.mobile',
      'voiceui.console',
      'voiceui.mobile'
    ],
    phone: [
      '+79101234567'
    ]
  }
});
