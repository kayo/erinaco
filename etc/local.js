define(['./voiceui'], function(voiceui){
  voiceui.keymap = {
    enter: {
      code: 'DISPLAY',
      title: 'эль'
    },
    cancel: {
      code: 'LOOP',
      title: 'эр'
    },
    power: {
      code: 'POWER'
    },
    number: {
      code: '1234567890'
    }
  };
  voiceui.offtime = 3 * 60 * 1000; // 3 minutes
  voiceui.device = 'default';

  return voiceui;
});
