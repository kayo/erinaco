define(['./voiceui'], function(voiceui){
  voiceui.keymap = {
    enter: {
      code: '*',
      title: 'звёздочку'
    },
    cancel: {
      code: '#',
      title: 'решётку'
    },
    number: {
      code: '1234567890'
    }
  };
  //voiceui.device = 'org.illumium.gsm_output';
  voiceui.device = 'gsmo';

  return voiceui;
});
