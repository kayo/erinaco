define({
  title: 'Параметры',
  group: {
    env: {
      title: 'Состояние климата',
      group: {
        air: {
          title: 'Температура в доме',
          units: 'degree',
          iface: 'dpc',
          hwdev: 'home',
          param: 'term_air',
          perms: 'ro'
        },
        oil: {
          title: 'Температура воды',
          units: 'degree',
          iface: 'dpc',
          hwdev: 'home',
          param: 'term_oil',
          perms: 'ro'
        },
        ext: {
          title: 'Температура во дворе',
          units: 'degree',
          iface: 'dpc',
          hwdev: 'home',
          param: 'term_ext',
          perms: 'ro'
        },
        htr: {
          title: 'Текущая мощность нагревателя',
          units: 'kwatt',
          iface: 'dpc',
          hwdev: 'home',
          param: 'htr',
          wpoll: '1M', /* set to device every minute */
          /* computable value */
          solve: [ /* Nearest matched condition will be selected */
            /* extremal control */
            'env.oil < env_ctl.oil.min', 2, /* water too cold: heater on */
            'env.oil > env_ctl.oil.max', 0, /* water too hot: heater off */
            /* normal control */
            'env_ctl.mode == 3', 'env_ctl.htr', /* manual mode: constant heater power */
            'env_ctl.mode == 1 && sys.power_tariff == 2 || env_ctl.mode == 2', [ /* automatic mode: economic or normal */
              'env.air < env_ctl.air.min', 3,
              'env.air < env_ctl.air.opt', 2,
              'env.air < env_ctl.air.max', 1,
              0
            ],
            0
          ]
        }
      }
    },
    env_ctl: {
      title: 'Управление климатом',
      group: {
        mode: {
          title: 'Режим работы',
          value: 1,
          oneof: {
            'Экономичный': 1,
            'Нормальный': 2,
            'Ручной': 3
          }
        },
        htr: {
          title: 'Мощность нагревателя',
          units: 'kwatt',
          value: 0,
          range: [0, 3]
        },
        air: {
          title: 'Температура воздуха',
          group: {
            opt: {
              title: 'Оптимальная температура',
              units: 'degree',
              value: 22,
              range: [1, 30]
            },
            max: {
              title: 'Максимальная температура',
              units: 'degree',
              value: 24,
              range: [1, 30]
            },
            min: {
              title: 'Минимальная температура',
              units: 'degree',
              value: 18,
              range: [1, 30]
            }
          }
        },
        oil: {
          title: 'Температура теплоносителя',
          group: {
            opt: {
              title: 'Оптимальная температура',
              units: 'degree',
              value: 35,
              range: [5, 30]
            },
            max: {
              title: 'Максимальная температура',
              units: 'degree',
              value: 55,
              range: [5, 30]
            },
            min: {
              title: 'Минимальная температура',
              units: 'degree',
              value: 7,
              range: [5, 30]
            },
            crt: {
              title: 'Критическая температура',
              units: 'degree',
              value: 4,
              range: [1, 7]
            }
          }
        }
      }
    },
    sys: {
      title: 'Состояние устройства',
      group: {
        term_intern: {
          title: 'Температура процессора',
          units: 'degree',
          iface: 'w1t',
          ucode: '28-0000029fbfe8',
          rpoll: '5S'
        },
        time: {
          title: 'Время',
          group: {
            year: {
              title: 'Год',
              iface: 'evt',
              event: 'time',
              units: 'year'
            },
            mon: {
              title: 'Месяц',
              iface: 'evt',
              event: 'time',
              units: 'mon',
              oneof: {
                'Январь': 1,
                'Февраль': 2,
                'Март': 3,
                'Апрель': 4,
                'Май': 5,
                'Июнь': 6,
                'Июль': 7,
                'Август': 8,
                'Сентябрь': 9,
                'Октабрь': 10,
                'Ноябрь': 11,
                'Декабрь': 12
              }
            },
            date: {
              title: 'Число',
              iface: 'evt',
              event: 'time',
              units: 'date'
            },
            day: {
              title: 'День недели',
              iface: 'evt',
              event: 'time',
              units: 'day',
              oneof: {
                'Понедельник': 1,
                'Вторник': 2,
                'Среда': 3,
                'Четверг': 4,
                'Пятница': 5,
                'Суббота': 6,
                'Воскресенье': 7
              }
            },
            hour: {
              title: 'Часы',
              iface: 'evt',
              event: 'time',
              units: 'hour'
            },
            min: {
              title: 'Минуты',
              iface: 'evt',
              event: 'time',
              units: 'min'
            },
            sec: {
              title: 'Секунды',
              iface: 'evt',
              event: 'time',
              units: 'sec'
            }
          }
        },
        power_tariff: {
          title: 'Энэрготариф',
          solve: [
            'sys.time.hour >= 7 && sys.time.hour < 23', 1, /* expensive */
            2 /* economical */
          ],
          oneof: {
            'Дневной': 1,
            'Ночной': 2
          }
        },
        modem: {
          title: 'Состояние модэма',
          group: {
            balance: {
              title: 'Баланс средств на счёте модэма',
              units: 'ruble',
              iface: 'gsm',
              type: 'ussd',
              req: '*100#',
              res: /^\D*(\d+).*$/,
              rpoll: '1H'
            },
            uplink: {
              title: 'Соединение с интернетом',
              iface: 'gsm',
              param: 'gprs.status',
              oneof: {
                'Не установлено': false,
                'Установлено': true
              },
              rpoll: '10S'
            }
          }
        }
      }
    },
    sys_ctl: {
      title: 'Управление устройством',
      group: {
        uplink: {
          title: 'Интернет соединение',
          value: false,
          oneof: {
            'Выключено': false,
            'Включено': true
          },
          iface: 'gsm',
          param: 'gprs'
        }
      }
    },
    alert: {
      title: 'Возможные проблемы',
      group: {
        oil: {
          title: 'Вода',
          oneof: {
            'слишком холодная': true,
            'в приемлемом состоянии': false
          },
          solve: [
            'env.oil < env_ctl.oil.crt', true,
            false
          ]
        },
        bil: {
          title: 'Средств на счёте модэма',
          oneof: {
            'слишком мало': true,
            'достаточно': false
          },
          solve: [
            'sys.modem.balance < 10', true,
            false
          ]
        }
      }
    }
  }
});
