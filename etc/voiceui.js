define({
  preset: 'russian',
  repeat: 3000,
  plural: 'n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2',
  units: {
    degree: ['градус', 'градуса', 'градусов'],
    kwatt: ['киловат', 'киловата', 'киловат'],
    ruble: ['рубль', 'рубля', 'рублей']
  },
  phrase: {
    enter: 'Вас приветствует умный дом...',
    leave: 'Умный дом прощается с вами, до новых встреч...',
    error: 'Соединение с сервисом почему-то недоступно',
    group: {
      head: 'текущий раздел: {node.title}.',
      item: 'чтобы выбрать: {node.title}, нажмите: {key.number.title}.',
      back: 'чтобы вернуться назад, нажмите: {key.cancel.title}.',
      loss: 'неверный выбор'
    },
    param: {
      oneof: {
        get: '{node.title}: {value.title}',
        set: {
          select: 'чтобы выбрать: {value.title}, нажмите: {key.number.title}',
          defval: 'чтобы сбросить значение, нажмите: {key.enter.title}',
          badval: 'значение не выбрано, повторите снова'
        },
        res: {
          done: 'значение успешно изменено',
          fail: 'не удалось изменить значение'
        }
      },
      range: {
        get: '{node.title}: {node.value} {node.value_units}',
        set: {
          change: 'чтобы изменить значение, введите число от: {node.range[0]} до: {node.range[1]}, затем нажмите: {key.enter.title}',
          cancel: 'чтобы отменить ввод, нажмите: {key.cancel.title}',
          defval: 'чтобы сбросить значение, просто нажмите {key.enter.title}, не вводя числ+а',
          badval: 'введённое значение некорректно, повторите снова'
        },
        res: {
          done: 'значение успешно изменено',
          fail: 'не удалось изменить значение'
        }
      }
    }
  },
  patsub: {
    'неопределено': /(undefined|null)/g,
    'вады-ы': /воды/g
  }
});
