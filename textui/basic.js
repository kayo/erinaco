define(['ndbus', '../voiceui/utils', 'CONFIG/access'], function(DBus, Utils, access){
  function Basic(opt){
    this.users = access;

    this.bus = DBus(opt.bus == ':system:' ? true : opt.bus ? opt.bus : false);
  }

  Basic.prototype = {
    req: function(sess, text){
      var self = this;

      text = text.split(/\s+/);

      var action = text[0],
          path = text[1] || '',
          value = text[2];

      path = path.replace(/[^a-zA-Z0-9\_\.]/g, '');
      path = '/' + path.replace(/\./g, '/');

      switch(action){
        case 'g':
        case 'get':
        Utils.retrieveNode(self.bus, path, function(err, node){
          if(err){
            self.res(sess, "Unable to retrieve node: " + path + '(' + err.message + ')');
            return;
          }
          self.res(sess, formatNode(node));
        });
        break;

        case 's':
        case 'set':
        changeValue(self.bus, path, value, function(err){
          self.res(sess, err || 'OK');
        });
        break;

        case 'q':
        case 'quit':
        process.exit(0);
        break;

        case '?':
        default:
        self.res(sess, [
          'get(g) [path.to.param]',
          'set(s) path.to.param value',
          'quit(q)'
        ].join('\n'));
        break;
      }
    },
    res: function(){
      throw new Error('Response method does not implemented.');
    },
    start: function(done){
      done();
    },
    stop: function(done){
      done();
    }
  };

  function formatNode(node, indent){
    indent = indent || '';
    var out = [indent + node.title + '(' + node.path.replace(/^\//, '').replace(/\//g, '.') + ')' + ':'];
    if(node.group){
      for(var i = 0; i < node.group.length; i++){
        out.push(formatNode(node.group[i], indent + ' '));
      }
    }else{
      if('value' in node){
        out[0] += node.value;
      }
      //if(indent){
      if(node.range){
        out[0] += ' [' + node.range.join(',') + ']';
      }
      if(node.oneof){
        out[0] += ' [' + formatOneof(node.oneof) + ']';
      }
    }
    return out.join('\n');
  }

  function formatOneof(oneof){
    var out = [];
    for(var title in oneof){
      out.push(title + ':' + oneof[title]);
    }
    return out.join(',');
  }

  function changeValue(bus, path, value, done){
    bus.proxy('org.illumium.erinaco', path, 'org.illumium.erinaco.Param', function(err, param){
      if(err){
        done(err);
        return;
      }
      if(!param.SetValue){
        done(new Error('Read only'));
        return;
      }
      param.SetValue(value, function(err){
        if(err){
          done(err);
          return;
        }
        done();
      });
    });
  }

  return Basic;
});
