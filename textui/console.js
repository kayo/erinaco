define(['util', './basic', 'readline'], function(util, Basic, ReadLine){
  function Console(opt){
    Basic.call(this, opt);

    var user = process.env.SUDO_USER || process.env.USER;

    if(!user){
      console.log('Access denied');
      process.exit(-1);
    }

    user = this.users[user];

    if(!user || !user.perm || user.perm.indexOf(opt.main) < 0){
      console.log('Access denied');
      process.exit(-1);
    }

    this.user = user;
  }

  util.inherits(Console, Basic);

  Console.prototype.start = function(done){
    var self = this;

    Basic.prototype.start.call(this, function(){
      var rl = self._rl = ReadLine.createInterface({
        input: process.stdin,
        output: process.stdout
      });

      rl.setPrompt('Erinaco:> ');
      rl.prompt();

      rl.on('line', function(text){
        self.req('cli', text);
      });

      done();
    });
  };

  Console.prototype.stop = function(done){
    var self = this;

    Basic.prototype.stop.call(self, function(){
      done();
    });
  };

  Console.prototype.res = function(to, text){
    var self = this,
        rl = self._rl;

    console.log(text);
    rl.prompt();
  };

  return Console;
});
