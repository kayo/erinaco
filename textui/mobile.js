define(['util', './basic', '../voiceui/ofono', 'CONFIG/mobile'], function(util, Basic, Ofono, config){
  function Mobile(opt){
    Basic.call(this, opt);

    this._main = opt.main;

    this._opts = config;
  }

  util.inherits(Mobile, Basic);

  Mobile.prototype.access = function(phone){
    for(var name in this.users){
      var user = this.users[name];
      if(user.perm.indexOf(this._main) > -1 &&
         user.phone.indexOf(phone) > -1){
        return true;
      }
    }
    return false;
  };

  Mobile.prototype.start = function(done){
    var self = this;

    Basic.prototype.start.call(self, function(){
      self._ofono = Ofono();

      self._ofono.on('modem', function(modem){
        self._modem = modem;
        modem.state(true);

        modem.on('sms', function(from, text){
          if(self.access(from)){
            self.req(from, text);
          }
        });
      });

      done();
    });
  };

  Mobile.prototype.stop = function(done){
    var self = this;

    Basic.prototype.stop.call(self, function(){
      done();
    });
  };

  Mobile.prototype.res = function(to, text){
    var self = this;

    self._modem.sms(to, text);
  };

  return Mobile;
});
