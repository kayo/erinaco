define(['ndbus', './speech', './interact', 'CONFIG/access'], function(DBus, Speech, Interact, access){
  function Basic(opt){
    this.users = access;

    this.bus = DBus(opt.bus == ':system:' ? true : opt.bus ? opt.bus : false);
    this.cache = opt.cache_path;

    this._sensor = null;
    this._speech = null;
    this._session = null;
  }

  Basic.prototype.start = function(done){
    var self = this,
        opts = self._opts;

    console.log('voiceui started');

    done();
  };

  Basic.prototype.stop = function(done){
    var self = this;

    console.log('voiceui stopped');

    done();
  };

  Basic.prototype.begin = function(){
    var self = this;

    console.log('begin voiceui session');

    self._sensor.setEncoding('ascii');

    self._speech = Speech(self._opts);

    self._session = new Interact();
    self._session.init(self._opts, self.bus, self._sensor, self._speech, function(err){
      self.end();
    });

    self._session.nav('/');
  };
  
  Basic.prototype.end = function(){
    var self = this;

    console.log('end voiceui session');

    self._session.done();
    self._session = null;

    self._speech.end();
    self._speech = null;
  };

  return Basic;
});
