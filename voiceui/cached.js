define(['ndbus', './cacher', './speech', './utils'], function(DBus, Cacher, Speech, Utils){
  var cache,
      bus;

  function Cached(opts){
    cache = opts.cache_path;
    bus = DBus(opts.bus == ':system:' ? true : opts.bus ? opts.bus : false);
    require(['CONFIG/' + opts.use_config], main);
  }

  function main(opts){
    var phrase = opts.phrase,
        patsub = opts.patsub,
        keymap = opts.keymap,
        units = Utils.makeUnits(opts.plural, opts.units),

        speaker = Cacher({
          keygen: {
            prefix: (opts.format || '') + (opts.rate || '') + (opts.channels || '')
          },
          storage: {
            path: cache,
            fext: ''
          }
        }).stream(Speech(opts));

    speaker.on('data', function(){
      /* hook */
    });

    render(phrase.enter);
    render(phrase.leave);

    function render(phrase, node, subs){
      for(var chunk,
              chunks = Utils.producePhrase(phrase, keymap, node, subs, patsub);
        chunk = chunks.shift(); ){
        console.log('>>', chunk);
        speaker.write(chunk);
      }
    }

    renderNode('/');

    function renderNode(path){
      Utils.retrieveNode(bus, path, function(err, node){
        if(err){
          return;
        }
        if(node.group){
          renderGroup(node);
        }else{
          renderParam(node);
        }
      });
    }

    function renderGroup(node){
      var phrases = phrase.group,
          group = node.group;

      render(phrases.head, node);

      for(var i = 0; i < group.length; i++){
        render(phrases.item, node, {
          key: {
            number: {
              title: i + 1
            }
          },
          node: group[i]
        });
        renderNode(group[i].path);
      }

      if(node.path != '/'){
        render(phrases.back, node);
      }
    }

    function renderParam(node){
      var type = 'oneof' in node ? 'oneof' : 'range',
          phrases = phrase.param[type];

      if(node.value === ''){
        node.value = null;
      }

      if('range' in node){
        for(var step = node.range.length > 2 ? node.range[2] : 1,
                value = node.range[0]; value <= node.range[1]; value += step){
          render(phrases.get, node, {
            node: {
              value: value,
              value_units: units(node, value)
            }
          });
        }
      }

      if('oneof' in node){
        for(var title in node.oneof){
          render(phrases.get, node, {
            value: {
              title: title
            }
          });
        }
      }

      if(node.change){
        if('range' in node){
          render(phrases.set.change, node);
          render(phrases.set.cancel, node);
        }

        if('oneof' in node){
          var i = 0;
          for(var title in node.oneof){
            i++;
            render(phrases.set.select, node, {
              value: {
                title: title
              },
              key: {
                number: {
                  title: i
                }
              }
            });
          }
        }

        render(phrases.set.defval, node);

        render(phrases.set.badval, node);
        render(phrases.res.done, node);
        render(phrases.res.fail, node);
      }

      render(phrase.group.back, node);
    }
  }

  return Cached;
});
