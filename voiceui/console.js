define(['./basic', 'pulseaudio', 'util', 'stream', 'readline', 'CONFIG/console'], function(Basic, PulseAudio, util, Stream, ReadLine, config){
  function Console(opt){
    Basic.call(this, opt);

    var user = process.env.SUDO_USER || process.env.USER;

    if(!user){
      console.log('Access denied');
      process.exit(-1);
    }

    user = this.users[user];

    if(!user || !user.perm || user.perm.indexOf(opt.main) < 0){
      console.log('Access denied');
      process.exit(-1);
    }

    this.user = user;

    this._opts = config;
  }

  util.inherits(Console, Basic);

  Console.prototype.start = function(done){
    var self = this;

    Basic.prototype.start.call(this, function(){
      self._rl = ReadLine.createInterface({
        input: process.stdin,
        output: process.stdout
      });

      self._rl.setPrompt('Erinaco[1234567890*#]:> ');

      done();

      self.begin();
    });
  };

  Console.prototype.begin = function(){
    var self = this;

    self._sensor = Stream.PassThrough();

    self._rl.addListener('line', self._rlev = function(text){
      text = text.replace(/\n$/, '');

      if(/^q(?:uit)?$/.test(text)){
        self.end();
        return;
      }
      
      if(/^[1234567890\*\#]$/.test(text)){
        self._sensor.write(text);
      }

      self._rl.prompt();
    });

    self._rl.prompt();

    Basic.prototype.begin.call(self);
  };

  Console.prototype.end = function(){
    var self = this;

    Basic.prototype.end.call(self);

    self._rl.removeListener('line', self._rlev);
    delete self._rlev;

    self._sensor.end();
    delete self._sensor;

    self.stop(function(){
      process.exit(0);
    });
  };

  return Console;
});
