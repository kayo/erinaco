define(function(){
  function Base(){}

  Base.prototype = {
    $dive: function(state, args){ /* change state */
      var self = this,
          handler;
      if(self.$state){
        self.$emit('$leave', args);
      }
      self.$state = state;
      self.$emit('$enter', args);
    },
    $emit: function(event, args){ /* emit event */
      args = args || [];
      var self = this,
          events = self.$spec[self.$state],
          handler = events[event] || events.$other;
      if(handler){
        if(handler == events.$other){
          args.unshift(event);
        }
        return handler.apply(self, args);
      }
    }
  };

  /*
   * {
   *   state: {
   *     event: function
   *   }
   * }
   *
   */
  return function(seed, spec, prot){
    if('object' == typeof seed){
      prot = spec;
      spec = seed;
      seed = null;
    }
    function FSM(args){
      if(this.$init){
        this.$init.apply(this, arguments);
      }
      if(this.$seed){
        this.$dive.call(this, this.$seed, args);
      }
    }
    var proto = FSM.prototype = new Base();
    if(prot){
      for(var i in prot){
        (i in proto) || (proto[i] = prot[i]);
      }
    }
    proto.$seed = seed;
    proto.$spec = spec;
    return FSM;
  };
});
