define(['./fsm', 'path', './utils', 'debug'], function(FSM, Path, Utils, Debug){
  var debug = Debug('interact');

  return FSM({
    group: {
      reply: function(){
        var self = this,
            phrase = self._phrase.group,
            node = self._node,
            group = node.group;

        self.say(phrase.head);

        for(var i = 0; i < group.length; i++){
          self.say(phrase.item, {
            key: {
              number: {
                title: i + 1
              }
            },
            node: group[i]
          });
        }

        if(node.path != '/'){
          self.say(phrase.back);
        }
      },
      input: function(key, type){
        var self = this,
            node = self._node,
            group = node.group,
            phrase = self._phrase.group;

        switch(type){
          case 'number':
          key = key | 0;
          if(key > 0 && key <= group.length){
            self.nav(group[key - 1].path);
          }else{
            self.say(phrase.loss);
          }
          break;

          case 'cancel':
          self.nav(node.path, true);
          break;
        }
      }
    },
    param_oneof: {
      reply: function(){
        var self = this,
            phrase = self._phrase.param.oneof,
            node = self._node,
            oneof = node.oneof;

        self.say(phrase.get, {
          value: {
            title: Utils.oneofTitle(node.value, oneof)
          }
        });

        if(node.change){
          var i = 0;
          for(var title in oneof){
            i++;
            if(oneof[title] != node.value){
              self.say(phrase.set.select, {
                value: {
                  title: title
                },
                key: {
                  number: {
                    title: i
                  }
                }
              });
            }
          }
          self.say(phrase.set.defval, node);
        }

        self.say(self._phrase.group.back);
      },
      result: function(state){
        var self = this,
            node = self._node,
            phrase = self._phrase.param.oneof;

        self.say(phrase.res[state ? 'done' : 'fail']);

        self.nav(node.path);
      },
      badval: function(){
        var self = this,
            node = self._node,
            phrase = self._phrase.param.oneof;

        self.say(phrase.set.badval);
      },
      input: function(key, type){
        var self = this,
            node = self._node,
            oneof = node.oneof,

            value;

        switch(type){
          case 'enter': /* reset logic */
          value = node.defval;

          case 'number': /* select logic */
          if(node.change){
            if(value === undefined){
              key = key | 0;

              var i = 0;

              for(var title in oneof){
                if(++i == key){
                  value = oneof[title];
                  break;
                }
              }
            }

            if(value !== undefined){
              node.change(value, function(state){
                self.$emit('result', [state]);
              });
            }else{
              self.$emit('badval');
            }
          }
          break;

          case 'cancel': /* exit logic */
          self.nav(node.path, true);
          break;
        }
      }
    },
    param: {
      $enter: function(){
        this._value = '';
      },
      $leave: function(){
        delete this._value;
      },
      reply: function(){
        var self = this,
            phrase = self._phrase.param.range,
            node = self._node;

        if(node.value === ''){
          node.value = null;
        }

        self.say(phrase.get);

        if(node.change){
          self.say(phrase.set.change);
          self.say(phrase.set.cancel);
          self.say(phrase.set.defval);
        }

        self.say(self._phrase.group.back);
      },
      result: function(state){
        var self = this,
            node = self._node,
            phrase = self._phrase.param.range;

        self.say(phrase.res[state ? 'done' : 'fail']);

        self.nav(node.path);
      },
      badval: function(){
        var self = this,
            node = self._node,
            phrase = self._phrase.param.range;

        self.say(phrase.set.badval);
      },
      input: function(key, type){
        var self = this,
            node = self._node,
            oneof = node.oneof;

        switch(type){
          case 'number': /* append number logic */
          if(node.change){
            self._value += key;
          }
          break;

          case 'enter': /* reset/change logic */
          if(node.change){
            if(self._value == ''){
              self._value = node.defval;
            }

            var value = self._value | 0;

            if(value < node.range[0] || value > node.range[1]){
              self.$emit('badval');
              self._value = '';
              break;
            }

            node.change(value, function(state){
              self.$emit('result', [state]);
            });
          }
          break;

          case 'cancel': /* exit logic */
          if(self._value){ // at the first time reset value only
            self._value = '';
          }else{ // if no value go back
            self.nav(node.path, true);
          }
          break;
        }
      }
    }
  }, {
    init: function(config, bus, input, output, exit){
      var self = this;

      self._bus = bus;
      self._exit = exit;

      self._keymap = config.keymap;
      self._phrase = config.phrase;
      self._patsub = config.patsub;

      self.units = Utils.makeUnits(config.plural, config.units);

      self._in = input;
      self._out = output;

      self._repeat = config.repeat || 0;

      self._out.addListener('drain', self._ondrn = function(){
        self.idle(true);
      });

      self._in.addListener('data', self._onkey = function(key){
        self.key(key);
      });

      if(self._phrase.enter){
        self.say(self._phrase.enter);
      }
    },
    done: function(){
      var self = this;

      self.say(false);
      if(self._phrase.leave){
        self.say(self._phrase.leave);
      }

      self._in.removeListener('data', self._onkey);
      self._out.removeListener('drain', self._ondrn);
    },
    key: function(key){
      var self = this,
          map = self._keymap,
          type;

      if(map){
        for(var name in map){
          if(map[name].code.indexOf(key) != -1){
            type = name;
            break;
          }
        }
      }

      debug('key:', key, type);

      if(type){
        self.say(false);

        self.$emit('input', [key, type]);
      }
    },
    nav: function(path, back){
      var self = this;

      if(back){
        path = Path.dirname(path);
      }

      debug('nav:', path);

      Utils.retrieveNode(self._bus, path, function(err, node){
        if(err){
          self.say(self._phrase.error);
          self._exit(err);
          return;
        }

        var type = 'value' in node ? 'param' : 'group';

        type += 'oneof' in node ? '_oneof' : '';
        //type += 'range' in node ? '_range' : '';

        node.value_units = self.units(node);
        self._node = node;

        self.$dive(type);
        self.$emit('reply');
      });
    },
    idle: function(on){
      var self = this;

      if(self._repeater){
        clearTimeout(self._repeater);
        self._repeater = null;
      }

      if(on && self._repeat){
        self._repeater = setTimeout(function(){
          self.$emit('reply');
        }, self._repeat);
      }
    },
    say: function(phrase, subs){
      var self = this;

      debug('say:', phrase);

      self.idle(false);

      if(phrase === false){
        self._out.shift();
        return;
      }

      if('string' !== typeof phrase){
        throw new Error('Phrase must be a string.');
      }

      for(var chunk, chunks = Utils.producePhrase(phrase, self._keymap, self._node, subs, self._patsub);
        chunk = chunks.shift(); self._out.queue(chunk));
    }
  });
});
