define(['net', 'stream', 'util'], function(Net, Stream, util){
  function Lirc(opts){
    if(!(this instanceof Lirc)){
      return new Lirc(opts);
    }

    var self = this;

    Stream.Transform.call(self);

    self.$ = Net.Socket();
    self.$.connect(opts && opts.path || '/var/run/lirc/lircd');

    self.$.on('connect', function(){
      self.$.pipe(self);
    });

    //self.on('end', function(){
    //  self.$.end();
    //});
  }

  util.inherits(Lirc, Stream.Transform);

  Lirc.prototype._transform = function(data, enc, done){
    var self = this;

    data = data.toString('ascii').split(/\s+/).slice(1, 3);
    if(data[0] == '00'){
      self.push(data[1]);
    }
    done();
  };

  return Lirc;
});
