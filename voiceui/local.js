define(['./basic', 'pulseaudio', 'util', 'stream', './lirc', 'CONFIG/local'], function(Basic, PulseAudio, util, Stream, Lirc, config){
  function Local(opt){
    Basic.call(this, opt);

    this._opts = config;
    this._offtimer = null;
  }

  util.inherits(Local, Basic);

  Local.prototype.start = function(done){
    var self = this,
        opts = self._opts;

    Basic.prototype.start.call(this, function(){
      self._ir = Lirc({
        path: self._opts.lircd
      });

      self._ir.on('data', function(text){
        if(self._offtimer){
          clearTimeout(self._offtimer);
          self._offtimer = null;
        }
        if(text == opts.keymap.power.code){
          if(!self._sensor){
            self._sensor = self._ir;
            self.begin();
          }else{
            self.end();
          }
        }
        if(self._sensor && opts.offtime){
          self._offtimer = setTimeout(function(){
            self.end();
          }, opts.offtime);
        }
      });
      done();
    });
  };

  Local.prototype.begin = function(){
    var self = this;

    Basic.prototype.begin.call(self);
  };

  Local.prototype.end = function(){
    var self = this;

    Basic.prototype.end.call(self);

    self._sensor = null;
  };

  return Local;
});
