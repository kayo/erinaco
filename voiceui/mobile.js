define(['./basic', 'util', './ofono', 'CONFIG/mobile', 'ndbus', 'stream'], function(Basic, util, Ofono, config, DBus, Stream){
  function Mobile(opt){
    Basic.call(this, opt);

    this._main = opt.main;
    this._opts = config;
  }

  util.inherits(Mobile, Basic);

  Mobile.prototype.access = function(phone){
    for(var name in this.users){
      var user = this.users[name];
      if(user.perm.indexOf(this._main) > -1 &&
         user.phone.indexOf(phone) > -1){
        return true;
      }
    }
    return false;
  };

  Mobile.prototype.start = function(done){
    var self = this;

    Basic.prototype.start.call(this, function(){
      self._ofono = Ofono();

      self._ofono.on('modem', function(modem){
        modem.state(true);

        modem.on('call', function(call){
          if(self.access(call.from)){
            call.once('active', function(){
              console.log('active');

              self._call = call;

              call.once('remove', function(){
                console.log('inactive');

                self.end();
                self._call = null;
              });

              self.begin();
            });
            console.log('call answer');
            call.answer();
          }else{
            console.log('call hangup');
            call.hangup();
          }
        });
      });

      self.bus.proxy('org.illumium.gsm', '/default', 'org.illumium.gsm.Audio', function(err, iface){
        self._gsma = iface;

        self.bus.proxy('org.illumium.gsm', '/default', 'org.illumium.gsm.Event', function(err, iface){
          self._dtmf = iface;

          done();
        });
      });
    });
  };

  Mobile.prototype.begin = function(){
    var self = this;

    self._sensor = Stream.PassThrough();

    console.log('ui begin');

    self._gsma.Attach(function(err){
      if(err){
        console.log(err);
        return;
      }

      console.log('ui gsma attached');

      self._dtmf.Received.on(function(event){
        self._sensor.write(event);
      });

      Basic.prototype.begin.call(self);
    });
  };

  Mobile.prototype.end = function(){
    var self = this;

    Basic.prototype.end.call(self);

    console.log('ui end');

    self._dtmf.Received.off();
    self._gsma.Detach();

    self._sensor.end();
    delete self._sensor;
  };

  return Mobile;
});
