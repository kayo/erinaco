define(['util', 'events', 'stream', 'fs', 'tty', 'ndbus', 'child_process'], function(UTIL, Events, Stream, FS, TTY, DBus, CP){
  var Inherits = UTIL.inherits,
      Emitter = Events.EventEmitter,
      bus = DBus(true);

  function VoiceCall(path, attr, modem){
    var self = this;

    Emitter.call(self);

    self.name = path;

    if(attr){
      self.attr = attr;

      if(attr.LineIdentification){
        self.from = attr.LineIdentification;
      }

      self.type = 'call';
    }else{
      self.type = 'dial';
    }

    function init(iface){
      iface.PropertyChanged.on(function(name, value){
        self.emit('change', name, value);
        attr[name] = value;

        if(name == 'LineIdentification'){
          self[self.type == 'call' ? 'from' : 'to'] = self.attr.LineIdentification;
        }

        console.log('call.change', name, value);
        if(name == 'State'){
          if(value == 'active'){
            self.emit('active');
          }
        }
      });

      iface.DisconnectReason.on(function(reason){
        iface.$();
        self.emit('cancel', reason);
      });

      modem.emit(self.type, self);
    }

    bus.proxy('org.ofono', path, 'org.ofono.VoiceCall', function(err, iface){
      if(err){
        return;////
      }

      self.$ = iface;

      if(attr){
        init(iface);
      }else{
        iface.GetProperties(function(err, attr){
          if(err){
            return;
          }
          self.attr = attr;
          init(iface);
        });
      }
    });
  }

  Inherits(VoiceCall, Emitter);

  VoiceCall.prototype.answer = function(){
    this.$.Answer();
  };
  VoiceCall.prototype.hangup = function(){
    this.$.Hangup();
  };

  VoiceCall.prototype.remove = function(){
    console.log('call removed');
    this.$.$();
    this.emit('remove');
  };


  function Context(path, attr, modem){
    var self = this;

    Emitter.call(self);

    self.name = path;
    self.attr = attr;

    bus.proxy('org.ofono', path, 'org.ofono.ConnectionContext', function(err, iface){
      self.$ = iface;

      iface.PropertyChanged.on(function(name, value){
        self.emit('change', name, value);
        attr[name] = value;

        console.log('ctx.change', name, value);
        if(name == 'Active'){
          if(value){
            self.emit('connected');
          }else{
            self.emit('disconnected');
          }
        }
      });

      self.emit('ready');
    });
  }

  Inherits(Context, Emitter);

  Context.prototype.connection = function(state){
    var self = this;

    if(self.$){
      connection();
    }else{
      self.once('ready', connection);
    }

    function connection(){
      if(state && !self.attr.Active){
        console.log('gprs up');
        self.$.SetProperty('Active', true);
      }else if(!state && self.attr.Active){
        console.log('gprs down');
        self.$.SetProperty('Active', false);
      }
    }
  };

  Context.prototype.remove = function(){
    this.$.$();
    this.emit('remove');
  };


  function Modem(path, attr, manager){
    var self = this;

    Emitter.call(self);

    self.name = path;
    self.attr = attr;

    function init(ifaces){
      if(ifaces.indexOf('org.ofono.VoiceCallManager') >= 0){
        bus.proxy('org.ofono', path, 'org.ofono.VoiceCallManager', function(err, iface){
          self.Calls = iface;
          self.calls = {};

          iface.CallAdded.on(function(path, attr){
            console.log('add call:', path);
            self.calls[path] = new VoiceCall(path, attr, self);
          });

          iface.CallRemoved.on(function(path){
            console.log('del call:', path);
            if(self.calls[path]){
              self.calls[path].remove();
              delete self.calls[path];
            }
          });

          self.emit('call.ready');
        });
      }

      if(ifaces.indexOf('org.ofono.MessageManager') >= 0){
        bus.proxy('org.ofono', path, 'org.ofono.MessageManager', function(err, iface){
          self.SMS = iface;

          iface.IncomingMessage.on(function(msg, opt){
            self.emit('sms', opt.Sender, msg, opt.SentTime);
          });

          self.emit('sms.ready');
        });
      }

      if(ifaces.indexOf('org.ofono.AudioSettings') >= 0){
        bus.proxy('org.ofono', path, 'org.ofono.AudioSettings', function(err, iface){
          self.Audio = iface;

          iface.PropertyChanged.on(function(name, value){
            console.log('AudioSettings', name, value);
            if('Active' === name){
              if(value){
                self.emit('audio.on', self.input, self.output);
              }else{
                self.emit('audio.off');
              }
            }
          });

          self.emit('audio.ready');
        });
      }

      if(ifaces.indexOf('org.ofono.ConnectionManager') >= 0){
        bus.proxy('org.ofono', path, 'org.ofono.ConnectionManager', function(err, iface){
          self.Connect = iface;

          self.Connect.GetProperties(function(err, attr){
            self.connect_attr = attr;

            self.Connect.PropertyChanged.on(function(key, val){
              self.connect_attr[key] = val;
              if(key == 'Attached'){
                self.emit(val ? 'gprs.attached' : 'gprs.detached');
              }
            });

            self.Connect.GetContexts(function(err, ctxs){
              self.contexts = {};

              var i,
                  path,
                  attr;

              for(i = 0; i < ctxs.length; i++){
                path = ctxs[i][0];
                attr = ctxs[i][1];

                self.contexts[path] = new Context(path, attr, self);
              }

              self.Connect.ContextAdded.on(function(path, attr){
                self.contexts[path] = new Context(path, attr, self);
              });

              self.Connect.ContextRemoved.on(function(path){
                self.contexts[path].remove();
                delete self.contexts[path];
              });

              self.emit('gprs.ready');
            });
          });
        });
      }

      if(ifaces.indexOf('org.ofono.SupplementaryServices') >= 0){
        bus.proxy('org.ofono', path, 'org.ofono.SupplementaryServices', function(err, iface){
          self.USSD = iface;

          self.USSD.GetProperties(function(err, attr){
            self.ussd_attr = attr;

            self.USSD.PropertyChanged.on(function(key, val){
              self.ussd_attr[key] = val;
            });

            if(self.ussd_attr.State == 'active'){
              self.USSD.Cancel(ready);
            }else{
              ready();
            }

            function ready(){
              self.emit('ussd.ready');
            }
          });
        });
      }
    }

    bus.proxy('org.ofono', path, 'org.ofono.Modem', function(err, iface){
      self.Modem = iface;

      iface.PropertyChanged.on(function(name, value){
        self.attr[name] = value;

        if('Interfaces' === name){
          init(value);
        }
      });

      if(self.attr.Interfaces){
        init(self.attr.Interfaces);
      }

      manager.emit('modem', self);
    });
  }

  Inherits(Modem, Emitter);

  Modem.prototype.state = function(state){
    var self = this,
        iface = self.Modem;

    if(arguments.length){
      if(state){
        iface.SetProperty('Powered', true, function(err){
          iface.SetProperty('Online', true, function(err){

          });
        });
      }else{
        iface.SetProperty('Online', false, function(err){
          iface.SetProperty('Powered', false, function(err){

          });
        });
      }
    }else{
      return self.attr.Powered && self.attr.Online;
    }
  };

  Modem.prototype.call = function(to, cb){
    var self = this;

    if(self.Calls){
      call();
    }else{
      self.once('call.ready', call);
    }

    function call(){
      self.Calls.Dial(to, '', function(err, path){
        if(err){
          cb(err);
          return;
        }
        new VoiceCall(path, null, self);
        self.once('dial', function(call){
          cb(null, call);
        });
      });
    }
  };

  Modem.prototype.sms = function(to, text){
    var self = this;

    if(self.SMS){
      send();
    }else{
      self.once('sms.ready', send);
    }

    function send(){
      self.SMS.SendMessage(to, text);
    }
  };

  Modem.prototype.ussd = function(req, res){
    var self = this;

    if(self.ussd_attr){
      ussd();
    }else{
      self.once('ussd.ready', ussd);
    }

    function ussd(){
      if(self.ussd_attr.State != 'idle'){
        res(new Error('Line Busy'));
      }
      console.log('ussd req', req);
      self.USSD.Initiate(req, function(err, type, data){
        if(err){
          res(err);
          return;
        }

        if(type == 'USSD'){
          console.log('ussd res', data);
          res(null, data);
          return;
        }

        res(new Error('Line Busy'));
      });

      //self.USSD.USSDReceived.once(function(msg){
        //console.log('ussd res', msg);
        //self.USSD.Cancel();
        //res(null, msg);
      //});
    }
  };

  Modem.prototype.gprs = function(state){
    var self = this;

    if(arguments.length < 1){
      if(!self.contexts){
        return false;
      }

      var context;
      for(var path in self.contexts){
        context = self.contexts[path];
        break;
      }

      if(!context){
        return false;
      }

      return context.attr.Active === true;
    }

    console.log('gprs.connection', state);

    if(self.contexts){
      gprs();
    }else{
      self.once('gprs.ready', gprs);
    }

    function gprs(){
      var context;
      for(var path in self.contexts){
        context = self.contexts[path];
        break;
      }
      if(!context){
        console.log('gprs contexts not found');
        return;
      }
      if(state && !context.attr.Active){
        if(self.connect_attr.Attached){
          up();
        }else{
          self.once('gprs.attached', function(){
            up();
          });
          if(!self.connect_attr.Powered){
            console.log('gprs power up');
            self.Connect.SetProperty('Powered', true);
          }else{
            console.warn('hmm... gprs powered but not attached');
          }
        }

        /*
        if(self.connect_attr.Powered){
          up();
        }else{
          console.log('gprs power up');
          self.Connect.SetProperty('Powered', true, function(er){
            if(er){
              console.log('error then power up gprs');
              return;
            }
            up();
          });
        }
         */

        function up(){
          console.log('gprs connect');
          context.connection(true);

          context.once('connected', function(){
            var settings = context.attr.Settings;

            if(settings && settings.Method == 'static' &&
              settings.Interface && settings.Address){
              var cmd = ['ifconfig', settings.Interface, 'up', settings.Address];
              if(settings.Netmask){
                cmd.push('netmask');
                cmd.push(settings.Netmask);
              }
              console.log(cmd.join(' '));
              CP.exec(cmd.join(' '), function(err, stdout, stderr){
                if(err){
                  console.error(err);
                }
                if(stdout){
                  console.log(stdout);
                }
                if(stderr){
                  console.error(stderr);
                }
              });
            }
            self.emit('connected');
          });
        }
      }
      if(!state && context.attr.Active){
        context.connection(false);

        //console.log('gprs power down');
        //self.Connect.SetProperty('Powered', false);

        context.once('disconnected', function(){
          self.emit('disconnected');
        });
      }
    }
  };

  Modem.prototype.disconnect = function(){
    var self = this;

    if(self.Connect){
      disconnect();
    }else{
      self.once('gprs.ready', disconnect);
    }

    function disconnect(){
      self.Connect.DeactivateAll();
    }
  };

  Modem.prototype.remove = function(){
    this.emit('remove');
  };

  function Manager(){
    if(!(this instanceof Manager)){
      return new Manager();
    }

    var self = this;

    Emitter.call(self);

    self.modems = {};

    manager();

    function manager(){
      bus.proxy('org.ofono', '/', 'org.ofono.Manager', function(err, iface){
        if(err){
          setTimeout(manager, 10000);
          return;
        }

        iface.GetModems(function(err, modems){
          var i = 0,
              modem,
              path,
              attr;

          for(; i < modems.length; ){
            modem = modems[i++];
            path = modem[0];
            attr = modem[1];

            self.modems[path] = new Modem(path, attr, self);
          }
        });

        iface.ModemAdded.on(function(path, attr){
          self.modems[path] = new Modem(path, attr, self);
        });

        iface.ModemRemoved.on(function(path){
          self.modems[path].remove();
          delete self.modems[path];
        });
      });
    }
  }

  Inherits(Manager, Emitter);

  return Manager;
});
