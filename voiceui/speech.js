define(['ndbus', 'util', 'events'], function(DBus, util, Events){
  function Speech(opts){
    if(!(this instanceof Speech)){
      return new Speech(opts);
    }

    Events.EventEmitter.call(this);

    var self = this,
        bus = DBus(true);

    self._speaker = null;

    bus.proxy('org.illumium.speech', '/', 'org.illumium.speech.Manager', function(err, manager){
      if(err){
        self.emit('error', err);
        return;
      }
      manager.GetInstance(opts.preset || 'default', opts.device || '', function(err, path){
        if(err){
          self.emit('error', err);
          return;
        }
        bus.proxy('org.illumium.speech', path, 'org.illumium.speech.Speaker', function(err, speaker){
          if(err){
            self.emit('error', err);
            return;
          }
          self._speaker = speaker;
          self._speaker.Deque.on(function(count){
            self.emit('deque', count);
            if(!count){
              self.emit('drain');
            }
          });
          self.emit('connection');
        });
      });
    });
  }

  util.inherits(Speech, Events.EventEmitter);

  Speech.prototype.queue = function(phrase){
    var self = this;

    if(self._speaker){
      self._speaker.Queue(phrase);
    }else{
      self.once('connection', function(){
        self.queue(phrase);
      });
      }
  };

  Speech.prototype.shift = function(count){
    var self = this;

    count = count || 0;

    if(self._speaker){
      self._speaker.Shift(count);
    }
  };

  Speech.prototype.end = function(){
    var self = this;
    
    if(self._speaker){
      self.shift();
      self._speaker.$();
      self._speaker = null;
    }
  };

  return Speech;
});
