define(function(){
  function oneofTitle(value, oneof){
    for(var title in oneof){
      if(oneof[title] === value){
        return title;
      }
    }
    return null;
  }

  function applyPatsub(phrase, patsub){
    for(var word in patsub){
      phrase = phrase.replace(patsub[word], word);
    }
    return phrase;
  }

  function producePhrase(phrase, keymap, node, subs, patsub){
    for(var i = 0,
            chunks = preparePhrase(phrase, {
              key: keymap || {},
              node: node || {}
            }, subs);
      i < chunks.length;
      chunks[i] = applyPatsub(chunks[i], patsub), i++);

    return chunks;
  }

  function substValues(subj, dict, path){
    path = path || '';
    dict = dict || {};

    if('object' == typeof subj){
      if(subj instanceof Array){
        for(var i = 0; i < subj.length; i++){
          substValues(subj[i], dict, path ? path + '[' + i + ']' : '');
        }
      }else{
        for(var n in subj){
          substValues(subj[n], dict, (path ? path + '.' : '') + n);
        }
      }
    }else if(path){
      dict[path] = subj;
    }

    return dict;
  }

  function preparePhrase(phrase/*, subst1, subst2, ..., substN*/){
    var i,
        piece,
        subs = {},
        result = [];

    for(i = 1; i < arguments.length; ){
      substValues(arguments[i++], subs);
    }

    //phrase = phrase.split(/\s*(\{)([^\}]*)\}\s*/);
    phrase = phrase.split(/(\{)([^\}]*)\}/);

    for(i = 0; i < phrase.length; ){
      piece = phrase[i++];

      if('{' === piece){
        piece = subs[phrase[i++]];

        if('function' === typeof piece){
          piece = piece();
        }

        if('string' !== typeof piece){
          piece = '' + piece;
        }
      }

      if(piece){
        result.push(piece);
      }
    }

    //return result;
    return [result.join('')];
  }

  function retrieveNode(bus, path, cb){
    var item = {
      path: path
    },
        errs = [],
        cnt = 0;

    function end(err){
      if(err){
        errs.push(err);
      }
      if(!--cnt){
        cb && cb(errs.length ? new Error(errs.join('\n')) : null, item);
      }
    }

    function explore_item(item, level){
      bus.explore('org.illumium.erinaco', item.path, function(err, node){
        if(err){
          end(err);
          return;
        }
        for(var path in node){
          var ifaces = node[path];
          if(path){
            if(level){
              var sub = {
                path: item.path + (item.path != '/' ? '/' : '') + path
              };
              (item.group = item.group || []).push(sub);
              explore_item(sub, level - 1);
            }
          }else{
            if('org.illumium.erinaco.Basic' in ifaces){
              bus.proxy('org.illumium.erinaco', item.path, 'org.illumium.erinaco.Basic', function(err, basic){
                if(err){
                  end(err);
                  return;
                }
                basic.GetTitle(function(err, title){
                  item.title = title;
                  end();
                });
              });
              ++cnt;
            }
            if('org.illumium.erinaco.Param' in ifaces && level){ // is parameter
              bus.proxy('org.illumium.erinaco', item.path, 'org.illumium.erinaco.Param', function(err, param){
                if(err){
                  end(err);
                  return;
                }
                if(param.GetValue){
                  item.readable = true;
                  param.GetValue(function(err, value){
                    item.value = value;
                    end();
                  });
                  ++cnt;
                }
                if(param.SetValue){
                  item.writable = true;
                  item.change = function(value, cb){
                    param.SetValue(value, function(err, ret){
                      if(err){
                        cb(false);
                        return;
                      }
                      cb(ret);
                    });
                  };
                }
                if(param.GetDefault){
                  param.GetDefault(function(err, value){
                    item.defval = value;
                    end();
                  });
                  ++cnt;
                }
                if(param.GetUnits){
                  param.GetUnits(function(err, units){
                    if(units != ''){
                      item.units = units;
                    }
                    end();
                  });
                  ++cnt;
                }
                if(param.GetRange){
                  param.GetRange(function(err, range){
                    if(range.length > 0){
                      item.range = range;
                    }
                    end();
                  });
                  ++cnt;
                }
                if(param.GetAvail){
                  param.GetAvail(function(err, oneof){
                    var ne = false;
                    for(var n in oneof){
                      ne = true;
                    }
                    if(ne){
                      item.oneof = oneof;
                    }
                    end();
                  });
                  ++cnt;
                }
                end();
              });
              ++cnt;
            }
          }
        }
        end();
      });
      ++cnt;
    }

    explore_item(item, 1);
  }

  function makeUnits(plural, units){
    plural = new Function('n', 'return ' + (plural || 'n==1 ? 0 : 1') + ';');
    units = units || {};

    return function format(node, value){
      var _units = '';

      if(value !== undefined){
        value = parseFloat(value);
      }else if(node.value !== undefined){
        value = parseFloat(node.value);
      }
      if('number' == typeof value && !isNaN(value) && node.units && units[node.units]){
        _units = units[node.units][plural(value)];
      }
      return _units;
    };
  }

  return {
    retrieveNode: retrieveNode,
    preparePhrase: preparePhrase,
    producePhrase: producePhrase,
    oneofTitle: oneofTitle,
    makeUnits: makeUnits
  };
});
